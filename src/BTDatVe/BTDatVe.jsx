import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Result from "./Result";

const BTDatVe = () => {
  return (
    <div className="container mt-5">
      <h1>BTDatVe</h1>
      <div className="row">
        <div className="col-8">
          <h1 className="display-4 text-center">ĐẶT VÉ XEM PHIM</h1>
          <div className="text-center p-3 font-weight-bold display-4 bg-dark text-white mt-3">
            SCREEN
          </div>
          <ChairList data ={data}/>
        </div>
      <div className="col-4">
        <Result />
      </div>
      </div>
    </div>
  );
};

export default BTDatVe;
